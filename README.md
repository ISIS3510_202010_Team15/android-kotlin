# ParkingLotAndroid

La APK se encuentra en la ruta: ParkingLot\app\build\outputs\apk\debug

Se puede registrar para crear un nuevo usuario y utilizar la aplicaci�n o iniciar con usuario *cspace* contrase�a *cspace*

Los c�digos posibles para realizar seguimiento son *00001* o *00002*

Se puede ingresar [aqu�](https://www.the-qrcode-generator.com/) para generar un c�digo QR para *00001* o *00002* y escanearlo directamente.

Si no hay internet, es posible ver el mapa si est� guardado en memoria cach�, pero se mostrar� un Toast indicando que no hay conexi�n. La lista de parqueaeros
y el perfil se mantienen almacenados localmente, por lo que si no hay internet se mantiene la vista, siempre y cuando no se borren los datos de la aplicaci�n.



